package com.climax.climax;

import com.climax.climax.services.FileStorageService;
import jakarta.annotation.Resource;
import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ClimaxApplication implements CommandLineRunner {
	@Resource
	FileStorageService fileStorageService;

	@Bean
	public ModelMapper modelMapper(){
		return new ModelMapper();
	}

	public static void main(String[] args) {
		SpringApplication.run(ClimaxApplication.class, args);
	}

	@Override
	public void run(String... arg) throws Exception{
		fileStorageService.init();

	}
}

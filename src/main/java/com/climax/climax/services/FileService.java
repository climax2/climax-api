package com.climax.climax.services;

import com.climax.climax.entities.FileEntity;
import com.climax.climax.exception.ApiException;
import com.climax.climax.exception.RessourceNotFoundException;
import com.climax.climax.repositories.FileRepository;
import com.climax.climax.utils.ClientUtil;
import com.climax.climax.utils.DataUtil;
import com.climax.climax.utils.FileUtil;
import com.climax.climax.utils.StatistiquesUtil;
import com.opencsv.CSVReader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class FileService {
    private FileRepository fileRepository;

    private FileStorageService fileStorageService;

    private ModelMapper modelMapper;

    public FileService(FileRepository fileRepository, FileStorageService fileStorageService, ModelMapper modelMapper) {
        this.fileRepository = fileRepository;
        this.fileStorageService = fileStorageService;
        this.modelMapper = modelMapper;
   }

   public List<StatistiquesUtil> getStatistiques(List<ClientUtil> clients){
       Hashtable<String, List<Integer>> stats = new Hashtable<>();

       clients.forEach(client -> {
           if (stats.containsKey(client.getProfessionClient())){
               List<Integer> salaireList = stats.get(client.getProfessionClient());

               salaireList.add(client.getSalaireClient());

               stats.put(client.getProfessionClient(), salaireList);
           }

           else {
               List<Integer> salaireList = new ArrayList<>();
               salaireList.add(client.getSalaireClient());
               stats.put(client.getProfessionClient(), salaireList);
           }
       });

       List<StatistiquesUtil> statistiques = new ArrayList<>();

       Enumeration<String> professions = stats.keys();

       while (professions.hasMoreElements()){
           String profession = professions.nextElement();
           List<Integer> salaireList = stats.get(profession);

           final Integer[] total = {0};

           salaireList.forEach(sal -> {
               total[0] += sal;
           });

           Double moyenne = (double) total[0] / salaireList.size();

           statistiques.add(new StatistiquesUtil(profession, moyenne));
       }

       return statistiques;
   }

    public DataUtil readCsvFile(String nameFile) throws Exception{
        Path filePath = Paths.get("uploads/" + nameFile);

        List<ClientUtil> clients = new ArrayList<>();

        try (Reader reader = Files.newBufferedReader(filePath)) {
            try (CSVReader csvReader = new CSVReader(reader)) {
                String[] line;

                while((line = csvReader.readNext()) != null){
                    clients.add(new ClientUtil(line[0], line[1], Integer.parseInt(StringUtils.trimAllWhitespace(line[2])), StringUtils.trimAllWhitespace(line[3]), Integer.parseInt(StringUtils.trimAllWhitespace(line[4]))));
                }
            }
        }

        return (new DataUtil(clients, getStatistiques(clients)));
    }

    public DataUtil readJsonFile(String nameFile) throws Exception{
        Path filePath = Paths.get("uploads/" + nameFile);

        List<ClientUtil> clients = new ArrayList<>();

        JSONParser parser = new JSONParser();

        try {
            Object obj = parser.parse(Files.newBufferedReader(filePath));

            JSONObject jsonObject = (JSONObject) obj;

            JSONArray data = (JSONArray) jsonObject.get("clients");

            for (Object client : data){
                org.json.JSONObject clientJson = new org.json.JSONObject(client.toString());
                clients.add(new ClientUtil(clientJson.getString("nomClient"), clientJson.getString("prenomClient"), clientJson.getInt("ageClient"), clientJson.getString("professionClient"), clientJson.getInt("salaireClient")));
            }


        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        return (new DataUtil(clients, getStatistiques(clients)));
    }

    public DataUtil readXmlFile(String nameFile){
        Path filePath = Paths.get("/uploads/" + nameFile);

        List<ClientUtil> clients = new ArrayList<>();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try {
            dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

            DocumentBuilder db = dbf.newDocumentBuilder();

            Document doc = db.parse("uploads/" + nameFile);

            doc.getDocumentElement().normalize();

            NodeList clientsList = doc.getElementsByTagName("client");

            for (int i = 0; i < clientsList.getLength(); i++) {
                Node node = clientsList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE){
                    Element element = (Element) node;

                    clients.add(new ClientUtil(
                            element.getElementsByTagName("nomClient").item(0).getTextContent(),
                            element.getElementsByTagName("prenomClient").item(0).getTextContent(),
                            Integer.parseInt(element.getElementsByTagName("ageClient").item(0).getTextContent()),
                            element.getElementsByTagName("professionClient").item(0).getTextContent(),
                            Integer.parseInt(element.getElementsByTagName("salaireClient").item(0).getTextContent())
                    ));
                }
            }
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (SAXException e) {
            throw new RuntimeException(e);
        }

        return (new DataUtil(clients, getStatistiques(clients)));
    }

    public FileUtil uploadData(MultipartFile file) throws Exception {
        Date now = new Date();

        String extensionFile = StringUtils.getFilenameExtension(file.getOriginalFilename());

        String nameFile = "Data_" + now.getDate() + "_" + now.getMonth() + "_" + now.getYear() + "_" + now.getHours() + "_" + now.getMinutes() +  "." + extensionFile;

        try{
            fileStorageService.save(file, nameFile);
        } catch (Exception e){
            throw new ApiException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

        FileEntity fileEntity = new FileEntity(null, nameFile, extensionFile);

        fileEntity = fileRepository.save(fileEntity);

        return modelMapper.map(fileEntity, FileUtil.class);
    }

    public List<FileUtil> getFiles(){
        return fileRepository.findAll().stream().map((file) -> modelMapper.map(file, FileUtil.class)).collect(Collectors.toList());
    }

    public DataUtil readData (Long idFile) throws Exception {
        FileEntity file = fileRepository.findById(idFile).orElseThrow(() -> new RessourceNotFoundException("File", "Id", idFile));

        switch (file.getExtensionFile()){
            case "csv" :
                return readCsvFile(file.getNameFile());
            case "json" :
                return readJsonFile(file.getNameFile());
            case "xml" :
                return readXmlFile(file.getNameFile());
            default:
                throw new ApiException(HttpStatus.BAD_REQUEST, "Ce type de format n'est pas encore pris en charge");
        }
    }

}

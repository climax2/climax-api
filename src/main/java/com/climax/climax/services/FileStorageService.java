package com.climax.climax.services;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class FileStorageService {
    private final Path root = Paths.get("uploads");

    public void init(){
        try{
            Files.createDirectories(root);
        } catch (IOException e){
            throw new RuntimeException("Impossible d'initialiser le repertoire des fichiers");
        }
    }

    public void save(MultipartFile file, String nameFile){
        try{
            Files.copy(file.getInputStream(), this.root.resolve(nameFile));
        } catch (Exception e){
            if (e instanceof FileAlreadyExistsException) throw new RuntimeException("Ce fichier existe déjà");

            throw new RuntimeException(e.getMessage());
        }
    }

    public Resource load(String fileName){
        try{
            Path file = root.resolve(fileName);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) return resource;

            else throw new RuntimeException("Impossible de récupérer le fichier" + fileName);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }


}

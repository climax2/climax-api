package com.climax.climax.controllers;

import com.climax.climax.services.FileService;
import com.climax.climax.utils.DataUtil;
import com.climax.climax.utils.FileUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/file/")
@CrossOrigin(origins = "*")
public class FileController {
    private FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping("")
    private ResponseEntity<FileUtil> uploadData(@RequestParam("file")MultipartFile file) throws Exception {
        return new ResponseEntity<>(fileService.uploadData(file), HttpStatus.CREATED);
    }

    @GetMapping("")
    private List<FileUtil> getFiles(){
        return fileService.getFiles();
    }

    @GetMapping("{idFile}")
    private ResponseEntity<DataUtil> getData(@PathVariable Long idFile) throws Exception {
        return new ResponseEntity<>(fileService.readData(idFile), HttpStatus.OK);
    }
}

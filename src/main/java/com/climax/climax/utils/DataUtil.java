package com.climax.climax.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DataUtil {
    private List<ClientUtil> clients;

    private List<StatistiquesUtil> statistiques;


}

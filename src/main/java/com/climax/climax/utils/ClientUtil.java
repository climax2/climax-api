package com.climax.climax.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClientUtil {
    private String nomClient;

    private String prenomClient;

    private Integer ageClient;

    private String professionClient;

    private Integer salaireClient;
}

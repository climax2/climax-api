package com.climax.climax.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StatistiquesUtil {
    private String intituleProfession;

    private Double moyenneSalaire;
}

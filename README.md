# Climax Statistiques App (API)
Application de generation de rapports statistiques sur les clients de la societe Climax.
Elle permettra de lire la liste des clients de la societe a travers des fichiers de differents types (CSV, JSON, XML, etc) puis de calculer la moyenne des salaires par type de profession.

## Fonctionnalites
- Telechargement et sauvegarde d'un fichier de donnees
- Chargement et lecture d'un fichier sauvegarde
- Generation de la moyenne de salaire par type de profession

## Technologies utilisees
- Langage : Java 17
- Framework : Spring Boot
- SGBD : MySQL

## Prerequis
- Java 17 ou ulterieur

## Installation & Configuration
- Cloner le depot git
```git clone https:://gitlab.com/climax2/climax-api.git```
- Acceder au repertoire du projet
- Creer votre base de donnees
- Modifier les proprietes de l'application dans le fihier ```application.properties```
- Lancer le serveur de Spring Boot

## Utilisation
- Voir la documentation a travers le lien suivant [Documentation](https:://documenter.getpostman.com/view/20489369/2s9YysCLz5) 